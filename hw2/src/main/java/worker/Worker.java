package worker;
import food.Food;
import animals.Animal;
import animals.interfaces.Voice;

/**
 * Класс рабочего зоопарка
 */
public class Worker {
    /* Имя */
    String name;

    /**
     * Конструктор рабочего без параметров;
     * Имя по умолчанию "Валера"
     */
    public Worker() {
        setName("Валера");
    }

    /**
     * Конструктор коровы с параметром "имя"
     * @param name имя
     */
    public Worker(String name) {
        setName(name);
    }

    /**
     * Сеттер имени
     * @param name имя
     */
    private void setName(String name) {
        this.name = name;
    }

    /**
     * Геттер имени
     * @return String имя
     */
    public String getName() {
        return this.name;
    }

    /**
     * Кормить животное
     * @param animal объект класса Animal
     * @param food объект класса Food
     */
    public void feed(Animal animal, Food food) {
        System.out.printf("Работник %s кормит животное %s едой %s\n",
                this.getName(), animal.getName(), food.getVerboseName());
        animal.eat(food);
    }

    /**
     * Рабочий просит животное поговорить
     * @param animal объект класса Animal
     * @param animalName имя животного (Animal.getName())
     */
    public void getVoice (Voice animal, String animalName) {
        System.out.printf("%s просит %s поговорить: %s\n",
                this.getName(), animalName, animal.voice());
    }
}

