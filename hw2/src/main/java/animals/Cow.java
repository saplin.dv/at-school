package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;

/**
 * Класс коровы
 */
public class Cow extends Herbivore implements Run, Voice {

    /**
     * Конструктор коровы без параметров;
     * Имя по умолчанию "Бурёнка"
     */
    public Cow () {
        this.setDefaultSatiety();
        this.setName("Бурёнка");
    }

    /**
     * Конструктор коровы с параметром "имя"
     * @param name имя
     */
    public Cow(String name) {
        this.setDefaultSatiety();
        this.setName(name);
    }

    /**
     * Реализация интерфейса "бегать";
     * Если у коровы достаточно сытости, она может выполнить действие и затратить энергию;
     * Если нет - в консоли отобразится предупреждение;
     * Стоимость действия - константа интерфейса energyCost
     */
    @Override
    public void run() {
        int energyCost = 5;
        if (this.satiety > energyCost) {
            System.out.printf("Корова %s бегает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    /**
     * Реализация интерфейса "голос";
     * Корова произносит что-то на своём языке
     * @return String фраза коровы
     */
    @Override
    public String voice() {
       return "Му-у-у";
    }
}
