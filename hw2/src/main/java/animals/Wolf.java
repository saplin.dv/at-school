package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;

/**
 * Класс Волка
 * Реализация методов и интерфейсов идентична реализации у травоядных
 * (смотри javadoc у классов Carnivore, Duck, Fish, Cow)
 */
public class Wolf extends Carnivore implements Run, Voice {

    public Wolf() {
        this.setDefaultSatiety();
        this.setName("Серый");
    }

    public Wolf(String name) {
        this.setDefaultSatiety();
        this.setName(name);
    }

    @Override
    public void run() {
        int energyCost = 5;
        if (this.satiety > energyCost) {
            System.out.printf("Волк %s бегает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    @Override
    public String voice() {
        return "Ау-у-у-у!";
    }
}
