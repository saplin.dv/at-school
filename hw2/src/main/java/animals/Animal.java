package animals;
import animals.interfaces.Sleep;
import food.Food;

/**
 * Абстрактный класс животного, который содержит общие для всех животных методы и реализует общие интерфейсы.
 */
public abstract class Animal implements Sleep {
    /* Сытость животного */
    int satiety;
    /* Имя животного */
    String name;

    /**
     * Абстрактный метод "есть", переопределён в сабклассах для разделения животных на хищников и травоядных.
     * @param food объект класса Food
     */
    public abstract void eat(Food food);

    /**
     * Внутренний метод увеличения шкалы "сытости"
     * @param value количество энергии, которая была получена за еду
     */
    protected void increaseSatiety(int value) {
        this.satiety += value;
    }
    /**
     * Внутренний метод уменьшения шкалы "сытости"
     * @param value количество энергии, которая была затрачена на действие
     */
    protected void decreaseSatiety(int value) {
        this.satiety -= value;
    }

    /**
     * Геттер значения сытости
     * @return int - текущая сытость животного
     */
    public int getSatiety() {
        return this.satiety;
    }

    /**
     * Внутренний метод установления сытости по умолчанию при создании животного
     */
    protected void setDefaultSatiety() {
        this.satiety = 20;
    }

    /**
     * Сеттер имени животного
     * @param name имя животного
     */
    public void setName (String name) {
        this.name = name;
    }
    /**
     * Геттер имени животного
     * @return String имя животного
     */
    public String getName() {
        return this.name;
    }

    /**
     * Отказ животного что-либо делать, так как сытости (энергии) не хватает на выполнение действия
     */
    public void satietyWarning() {
        System.out.printf("%s голодный(ая)... Надо покормить.\n", this.getName());
    }

    /**
     * Реализация общего для всех животных интерфейса "спать";
     * Если у животного достаточно сытости, оно может выполнить действие и затратить энергию;
     * Если нет - в консоли отобразится предупреждение;
     * Стоимость действия - константа интерфейса energyCost
     */
    public void sleep() {
        int energyCost = 6;
        if (this.satiety > energyCost) {
            System.out.printf("%s спит\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }
}
