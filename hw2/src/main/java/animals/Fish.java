package animals;
import animals.interfaces.Swim;

/**
 * Класс рыбы
 */
public class Fish extends Herbivore implements Swim {

    /**
     * Конструктор рыбы без параметров;
     * Имя по умолчанию "Немо"
     */
    public Fish() {
        this.setDefaultSatiety();
        this.setName("Немо");
    }

    /**
     * Конструктор рыбы с параметром "имя"
     * @param name имя
     */
    public Fish(String name) {
        this.setDefaultSatiety();
        this.setName(name);
    }

    /**
     * Реализация интерфейса "плавать";
     * Если у рыбы достаточно сытости, она может выполнить действие и затратить энергию;
     * Если нет - в консоли отобразится предупреждение;
     * Стоимость действия - константа интерфейса energyCost
     */
    @Override
    public void swim() {
        int energyCost = 3;
        if (this.satiety > energyCost) {
            System.out.printf("Рыба %s плавает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }
}
