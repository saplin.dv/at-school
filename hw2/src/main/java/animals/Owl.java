package animals;
import animals.interfaces.Fly;
import animals.interfaces.Voice;
/**
 * Класс Совы
 * Реализация методов и интерфейсов идентична реализации у травоядных
 * (смотри javadoc у классов Carnivore, Duck, Fish, Cow)
 */
public class Owl extends Carnivore implements Fly, Voice {

    public Owl() {
        this.setDefaultSatiety();
        this.setName("Филя");
    }

    public Owl(String name) {
        this.setDefaultSatiety();
        this.setName(name);
    }

    @Override
    public void fly() {
        int energyCost = 5;
        if (this.satiety > energyCost) {
            System.out.printf("Сова %s летает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    @Override
    public String voice() {
        return "Угу";
    }
}
