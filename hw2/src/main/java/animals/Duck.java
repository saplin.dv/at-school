package animals;
import animals.interfaces.Fly;
import animals.interfaces.Run;
import animals.interfaces.Swim;
import animals.interfaces.Voice;

/**
 * Класс утки
 */
public class Duck extends Herbivore implements Swim, Fly, Run, Voice {

    /**
     * Конструктор утки без параметров;
     * Имя по умолчанию "Утя"
     */
    public Duck() {
        this.setDefaultSatiety();
        this.setName("Утя");
    }

    /**
     * Конструктор утки с параметром "имя"
     * @param name имя
     */
    public Duck(String name) {
        this.setDefaultSatiety();
        this.setName(name);
    }

    /**
     * Реализация интерфейсов "плавать", "летать", "бегать";
     * Если у утки достаточно сытости, она может выполнить действие и затратить энергию;
     * Если нет - в консоли отобразится предупреждение;
     * Стоимость действия - константа интерфейса energyCost
     */
    @Override
    public void swim() {
        int energyCost = 3;
        if (this.satiety > energyCost) {
            System.out.printf("Утка %s плавает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    /** Смотри javadoc для swim() */
    @Override
    public void fly() {
        int energyCost = 5;
        if (this.satiety > energyCost) {
            System.out.printf("Утка %s летает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    /** Смотри javadoc для swim() */
    @Override
    public void run() {
        int energyCost = 5;
        if (this.satiety > energyCost) {
            System.out.printf("Утка %s бегает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    /**
     * Реализация интерфейса "голос";
     * Утка произносит что-то на своём языке
     * @return String фраза утки
     */
    @Override
    public String voice() {
        return "Кря-кря!";
    }
}
