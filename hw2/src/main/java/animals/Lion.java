package animals;

import animals.interfaces.Run;
import animals.interfaces.Voice;

/**
 * Класс Льва
 * Реализация методов и интерфейсов идентична реализации у травоядных
 * (смотри javadoc у классов Carnivore, Duck, Fish, Cow)
 */
public class Lion extends Carnivore implements Run, Voice {
    public Lion() {
        this.setDefaultSatiety();
        this.setName("Бонифаций");
    }

    public Lion(String name) {
        this.setDefaultSatiety();
        this.setName(name);
    }

    @Override
    public void run() {
        int energyCost = 5;
        if (this.satiety > energyCost) {
            System.out.printf("Лев %s бегает\n", this.getName());
            this.decreaseSatiety(energyCost);
        }
        else this.satietyWarning();
    }

    @Override
    public String voice() {
        return "Р-р-р мяу!";
    }
}
