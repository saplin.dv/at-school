package animals;
import food.Food;
import food.Meat;

import java.lang.invoke.WrongMethodTypeException;

/**
 * Абстрактный класс для хищных животных, которые питаются только мясом
 */
abstract class Carnivore extends Animal {
    /**
     * Реализация абстрактного метода для хищников;
     * Если животное кормят объектом класса Meat, животное ест еду и повышает свою сытость;
     * Если животное кормят объектом класса Grass, животное не ест, в консоль выводится предупреждение;
     * @param food объект класса Food
     */
    public void eat(Food food) {
        if (food instanceof Meat) {
            System.out.printf("%s ест еду %s\n", this.getName(), food.getVerboseName());
            this.increaseSatiety(food.getEnergyValue());
        }
        else System.out.println("xxx Внимание: Плотоядные животные не едят траву.");
    }
}
