package animals;
import food.Food;
import food.Grass;

import java.lang.invoke.WrongMethodTypeException;

/**
 * Абстрактный класс для травоядных животных, которые питаются только травой
 */
public abstract class Herbivore extends Animal {
    /**
     * Реализация абстрактного метода для травоядных;
     * Если животное кормят объектом класса Grass, животное ест еду и повышает свою сытость;
     * Если животное кормят объектом класса Meat, животное не ест, в консоль выводится предупреждение;
     * @param food объект класса Food
     */
    public void eat(Food food) {
        if (food instanceof Grass) {
            System.out.printf("%s ест еду %s\n", this.getName(), food.getVerboseName());
            this.increaseSatiety(food.getEnergyValue());
        }
        else System.out.println("xxx Внимание: Травоядные животные не едят мясо.");
    }

}
