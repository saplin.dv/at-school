package food;

/**
 * Класс еды - мясо
 */
public class Meat extends Food {

    /**
     * Конструктор класса
     * Устанавливает название "мясо" и энергетическую ценность по умолчанию = 9
     */
    public Meat() {
        setVerboseName("мясо");
        setEnergyValue(9);
    }

}
