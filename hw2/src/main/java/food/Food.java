package food;

/**
 * Абстрактный класс еды, который содержит общие для травы и мяса методы
 */
public abstract class Food {
    /* Энергетическая ценность (ЭЦ)*/
    int energyValue;
    /* Произносимое название (обобщённое) */
    private String verboseName;

    /**
     * Сеттер энергетической ценности
     * @param energyValue значение ЭЦ
     */
    public void setEnergyValue(int energyValue) {
        this.energyValue = energyValue;
    }

    /**
     * Геттер энергетической ценности
     * @return int значение ЭЦ
     */
    public int getEnergyValue() {
        return energyValue;
    }

    /**
     * Геттер названия
     * @return String название
     */
    public String getVerboseName() {
        return this.verboseName;
    }

    /**
     * Сеттер названия
     * @param verboseName название
     */
    public void setVerboseName(String verboseName) {
        this.verboseName = verboseName;
    }
}
