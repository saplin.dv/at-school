package food;

/**
 * Класс еды - трава
 */
public class Grass extends Food {

    /**
     * Конструктор класса
     * Устанавливает название "трава" и энергетическую ценность по умолчанию = 6
     */
    public Grass() {
        setVerboseName("трава");
        setEnergyValue(6);
    }

}
