import animals.interfaces.Swim;
import food.*;
import worker.Worker;
import animals.*;

import java.util.ArrayList;

public class Zoo {
    /**
     * Точка входа в программу Зоопарка
     * Рабочий может кормить животных, просить их поговорить;
     * Животные могут выполнять действия, затрачивая на это энергию (сытость);
     * Действие будет выполнено при наличии достаточных единиц сытости, если
     * сытости не достаточно животное выведет в консоль предупреждение;
     * Хищников можно кормить только мясом, травоядных - травой;
     * pound - массив животных, которые умеют плавать;
     * @param cmdLineArgs параметры командной строки (not used)
     */
    public static void main(String[] cmdLineArgs) {
        System.out.println("~~~ Зоопарк ~~~");

        /* Создание объектов */
        Worker worker = new Worker();
        Grass grass = new Grass();
        Meat meat = new Meat();
        Duck duck = new Duck();
        Fish fish = new Fish();
        Cow cow = new Cow();
        Wolf wolf = new Wolf();
        Owl owl = new Owl();
        Lion lion = new Lion();

        /* Рабочий */
        worker.feed(duck, grass);
        worker.feed(wolf, meat);
        worker.feed(lion, grass);
        System.out.println();

        worker.getVoice(cow, cow.getName());
//        worker.getVoice(fish, fish.getName());
        System.out.println();

        /* Пруд */
        ArrayList<Swim> pound = new ArrayList<>();
        pound.add(fish);
        pound.add(duck);

        pound.forEach(Swim::swim);
        System.out.println();

        /* Сытость */
        for (int i=0; i<2; i++) {
            owl.fly();
            owl.sleep();
        }
        System.out.printf("Сытость совы %s: %d\n", owl.getName(), owl.getSatiety());
        worker.feed(owl, meat);
        System.out.printf("Сытость совы %s: %d\n", owl.getName(), owl.getSatiety());
        owl.fly();
        System.out.printf("Сытость совы %s: %d\n", owl.getName(), owl.getSatiety());
    }
}
