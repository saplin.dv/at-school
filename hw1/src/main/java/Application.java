import model.Kotik;

/**
 * Реализация дз №1 - "Кошачья имитация"
 * @author saplin.dv
 * @version 1.0
 */
public class Application {
    /**
     * Точка входа в приложение.
     * @param cmdLineArgs - параметры при запуске (not used)
     * Для корректной работы операторов assert, надо добавить аргумент -ea к VM options в Run configuration
     */

    public static void main (String[] cmdLineArgs){
        Kotik cat1 = new Kotik(10, "Вася", 2, "Мау" );
        Kotik cat2 = new Kotik();
        System.out.printf("Имя котика: %s; Вес: %dкг\n", cat1.getName(), cat1.getWeight());
        cat1.liveAnotherDay();
        System.out.printf("Результат сравнения кошачьих диалектов: %b\n", cat1.getMeow().equals(cat2.getMeow()));
        System.out.printf("Количество созданных котиков: %d\n", Kotik.getCount());
    }
}
