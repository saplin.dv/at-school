package model;

/**
 * Класс котика. Имеет аттрибут:
 * cat_counter - счётчик инстансов класса (изначально 0);
 * Также объявляются:
 * name, meow, pretiness, weight, satiety - инициализируются в конструкторах.
 */

public class Kotik {
    private static int count = 0; // Счётчик котиков
    private String name; // Имя котика
    private String meow; // Диалект котика
    private int prettiness; // Красота котика
    private int weight; // Вес котика
    private int satiety; // Сытость котика

    /** Конструктор котиков с параметрами
     * @param prettiness красота (может быть только 10);
     * @param name имя;
     * @param weight вес (1-20 кг);
     * @param meow диалект (как котик говорит);
     * Сытость ставится по умолчанию на 50 (setSatiety50());
     * Счётчик котиков cat_counter увеличивается на 1.
     */
    public Kotik(int prettiness, String name, int weight, String meow) {
        setKotik(prettiness, name, weight, meow);
    }

    /** Конструктор котиков без параметров (с параметрами по умолчанию)
     * prettiness 10;
     * name "Мурка";
     * weight 7;
     * meow "мяу";
     * Сытость ставится по умолчанию на 50 (setSatiety50());
     * Счётчик котиков cat_counter увеличивается на 1.
     */
    public Kotik() {
        setKotik(10, "Мурка", 7, "мяу");
    }

    /* Вызов сеттеров для конструктора*/
    private void setKotik(int prettiness, String name, int weight, String meow) {
        setPrettiness(prettiness);
        setName(name);
        setWeight(weight);
        setMeow(meow);
        setSatiety50();
        count++;
    }

    /* Сеттер красоты */
    private void setPrettiness(int prettiness) {
        assert (prettiness == 10): "Введено неверное значение для красоты! Все котики красивы на 10/10!";
        this.prettiness = prettiness;
    }

    /* Геттер красоты */
    public int getPrettiness() {
        return this.prettiness;
    }

    /* Сеттер веса */
    private void setWeight(int weight) {
        assert (weight < 20 & weight > 1): "Введено неверное значение веса! Вес котиков должен быть от 1 до 20 кг!";
        this.weight = weight;
    }

    /* Геттер веса */
    public int getWeight() {
        return this.weight;
    }

    /* Сеттер языка */
    private void setMeow(String meow) {
        this.meow = meow;
    }

    /* Геттер языка */
    public String getMeow() {
        return this.meow;
    }

    /* Геттер счётчика котиков */
    public static int getCount() {
        return count;
    }

    /* Геттер имени */
    public String getName() {
        return this.name;
    }

    /* Сеттер имени */
    private void setName(String name) {
        this.name = name;
    }

    /* Геттер сытости */
    public int getSatiety() {
        return this.satiety;
    }

    /* Сеттер сытости на 50*/
    private void setSatiety50() {
        this.satiety = 50;
    }

    /* Сеттер сытости на указанное значение */
    private void incrementSatiety(int foodAmount) {
        if (foodAmount < 101 & foodAmount > 0) {
            this.satiety += foodAmount;
        }
        else throw new RuntimeException("Err: Что-то пошло не так...");
    }


    /* Три перегрузки метода eat() */

    /** По умолчанию кормит котика кормом на 70 единиц */
    public void eat() {
        eat(70, "корм");
    }

    /** Кормим котика на указанное количество сытости какой-то едой
     * @param foodAmount количество единиц еды (сытости); если передано больше 100 - уменьшается до 100.
     *                   Зачем котикам переедать, верно?
     */
    public void eat(int foodAmount) {
        if (foodAmount < 0) {
            System.out.println("=u.u= А где еда?.. ");
        }
        else if (foodAmount > 100) {
            foodAmount = 100;
        }

        incrementSatiety(foodAmount);
        System.out.printf(" =^.^= %s поел на %d \n", this.name, foodAmount);
    }

    /** Кормим котика на указанное количество сытости конкретной едой
     * @param foodAmount количество единиц еды (сытости); если передано больше 100 - уменьшается до 100.
     *                   Зачем котикам переедать, верно?
     * @param foodName название еды.
     */
    public void eat(int foodAmount, String foodName) {
        if (foodAmount < 0) {
            System.out.println("=u.u= А где еда?.. ");
        }
        else if (foodAmount > 100) {
            foodAmount = 100;
        }
        System.out.printf(" =^.^= %s поел %s на %d единиц сытости\n", this.name, foodName, foodAmount);
        incrementSatiety(foodAmount);
    }

    /** Понижение сытости котика за любое действие.
     * При измении декремента НАСТОЯТЕЛЬНО рекомендуется в методе checkSatiety() поставить сравнение сытости равное
     * декременту-1 (Например: декремент 8 => checkSatiety() {return this.satiety > 7;}) */
    private void decreaseSatietyBy10() {
        this.satiety -= 10;
    }

    /** Проверка сытости котика.
     * При измении значения для сравнение НАСТОЯТЕЛЬНО рекомендуется в методе decreaseSatiety() поставить значение
     * декремента равное значению для сравнения+1
     * (Например: Значение для сравнения 7 => decreaseSatiety(): {return this.satiety += 8;}) */
    private boolean checkSatiety() {
        return this.satiety > 9;
    }

    /* Попросить еды */
    private void askFood() {
        System.out.printf("=x.x= %s хочет кушать ||", this.name);
    }

    /* Мяу */
    public void meow() {
        System.out.printf("%s: %s", this.name, this.meow);
    }

    /** Методы sleep(), play(), chaseMouse(), dropMug(), digDown() отвечают за действия котика и практически идентичны.
     * Все методы возвращают true/false в зависимости от успеха действия:
     * Если проверка на сытость check.Satiety() true - действие считается выполненным, возвращаем true;
     * Если проверка на сытость check.Satiety() false - действие считается не выполненным, вызывается метод askFood(),
     * возвращаем false.
     */

    public boolean sleep() {
        if (this.checkSatiety()) {
            System.out.printf("=-.-= %s спит\n", name);
            this.decreaseSatietyBy10();
            return true;
        }
        else {
            askFood();
            return false;
        }
    }

    public boolean play() {
        if (this.checkSatiety()) {
            System.out.printf("=*.*= %s играет\n", name);
            this.decreaseSatietyBy10();
            return true;
        }
        else {
            askFood();
            return false;
        }
    }

    public boolean chaseMouse() {
        if (this.checkSatiety()) {
            System.out.printf("=>.>= %s преследует мышь\n", name);
            this.decreaseSatietyBy10();
            return true;
        }
        else {
            askFood();
            return false;
        }
    }

    public boolean dropMug() {
        if (checkSatiety()) {
            System.out.printf("=O.O= %s уронил кружку\n", name);
            this.decreaseSatietyBy10();
            return true;
        }
        else {
            askFood();
            return false;
        }
    }

    public boolean digDown() {
        if (checkSatiety()) {
            System.out.printf("=v.v= %s копает\n", name);
            this.decreaseSatietyBy10();
            return true;
        }
        else {
            askFood();
            return false;
        }
    }

    /** Метод имитации 1 дня для котика.
     * В цикле 24 итерации, каждая итерация соответствует одному часу условных суток.
     * В начале каждой итерации генерируется случайное число int H, которое и будет
     * указывать на то, какое действие котик совершает в этот час.
     * Если действие вернуло true (завершилось успешно) - понижаем сытость котика на 10. (decreaseSatietyBy10()).
     * В противном случае - кормим (котика eat())
     */
    public void liveAnotherDay() {
        for (int n=1; n<25; n++) {
            System.out.printf("Час %d:\t", n);

                int H = 5;                          // количество методов для выбора
                int choice = (int) (Math.random() * H + 1);

                switch (choice) {
                    case (1):
                        if (!this.sleep()) {
                            this.eat();
                        }
                        break;
                    case (2):
                        if (!this.play()) {
                            this.eat();
                        }
                        break;

                    case (3):
                        if (!this.chaseMouse()) {
                            this.eat();
                        }
                        break;

                    case (4):
                        if (!this.dropMug()) {
                            this.eat();
                        }
                        break;

                    case (5):
                        if (!this.digDown()) {
                            this.eat();
                        }
                        break;
                }

        }
    }

}
